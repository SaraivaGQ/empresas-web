import {BrowserRouter, Switch, Route} from 'react-router-dom';
import React, { Suspense } from 'react';

const Login = React.lazy(() => import("./Pages/Login/Login"));
const Home = React.lazy(() => import("./Pages/Home/Home"));
const EnterpriseDetails = React.lazy(() => import("./Pages/EnterpriseDetails/EnterpriseDetails"));

function App() {
  return (
    <Suspense fallback={<main className="background"></main>}>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Login}></Route>
          <Route path="/home" exact component={Home}></Route>
          <Route path="/enterprise/:id" exact component={EnterpriseDetails}></Route>
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
