import React, { useEffect, useState } from 'react';
import "./EnterpriseDetails.css";

import Helmet from 'react-helmet';
import Header from '../../Components/Header/Header';
import Enterprise from '../../Components/Enterprise/Enterprise';
import Loading from '../../Components/Loading/Loading';
import {useParams, useHistory} from 'react-router-dom';
import {get_enterprise, is_logged, logoff} from '../../services/api';

function EnterpriseDetails() {
  
  const { id } = useParams();
  const history = useHistory();
  const [data, set_data] = useState({});
  const [loading, set_loading] = useState(true);

  const get_details = () => {
    get_enterprise(id)
    .then(result => {
      set_data(result.enterprise);
      set_loading(false);
    })
    .catch(err => {
      history.push("/home")
    })
  }

  useEffect(get_details, [id, history])
  useEffect(() => {
    if (!is_logged()) {
      logoff()
    }
  }, []);

  return (
    <>
      <Helmet>
          <title>{data.enterprise_name || "Empresas - Detalhes"}</title>
      </Helmet>
      <main style={{justifyContent: "flex-start"}} className="background">
          <h1 className="hidden">Empresas - Home</h1>
          <Header isDetails title={data.enterprise_name}/>

          <div style={{alignItems:'center'}} className="content">
            <Enterprise id={data.id} typeName={data.description} img={data.photo ? "https://empresas.ioasys.com.br/"+data.photo : null} noLink className="enterprise-details"/>
          </div>
          <Loading visible={loading} />
      </main>
    </>
  );
}

export default EnterpriseDetails;