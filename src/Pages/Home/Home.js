import React, { useEffect, useState } from 'react';
import "./Home.css"
import Helmet from "react-helmet"
import Header from '../../Components/Header/Header';
import Enterprise from '../../Components/Enterprise/Enterprise';
import {is_logged, logoff} from '../../services/api';

function Home() {

  const [enterprises_list, set_enterprises] = useState(null);
  useEffect(() => {
    if (!is_logged()) {
      logoff()
    }
  }, []);

  return(
      <>
        <Helmet>
            <title>Empresas - Home</title>
        </Helmet>
        <main style={{justifyContent: "flex-start"}} className="background">
            <h1 className="hidden">Empresas - Home</h1>
            <Header onSearch={set_enterprises}/>

            <div className="content">
              {enterprises_list && enterprises_list.map(item => {
                return(
                  <Enterprise id={item.id}  name={item.enterprise_name} typeName={item.enterprise_type.enterprise_type_name} country={item.country} img={item.photo ? "https://empresas.ioasys.com.br/"+item.photo : null}></Enterprise>
                )
              })}
            </div>
            
            {(enterprises_list == null || enterprises_list.length === 0) && (
              <div className="content-alerts">
                {enterprises_list === null ? 
                "Clique na busca para iniciar."
                : 
                enterprises_list.length === 0 ?
                "Nenhuma empresa foi encontrada para a busca realizada." 
                : 
                ""}
              </div>
            )}
            
        </main>
      </>
  );
}

export default Home;