import React, { Fragment, useEffect, useState, useCallback } from 'react';
import Helmet from 'react-helmet';
import './Login.css';

import TextInput from "../../Components/TextInput/TextInput";
import Button from "../../Components/Button/Button";
import Loading from '../../Components/Loading/Loading';
import {GoMail} from 'react-icons/go';
import {AiOutlineUnlock} from 'react-icons/ai';
import {useHistory} from 'react-router-dom';

import {login} from "../../services/api";


function Login() {

    const [email, set_email] = useState("");
    const [password, set_password] = useState("");
    const [error, set_error] = useState(false);
    const [loading, set_loading] = useState(false);

    const history = useHistory();

    const on_login_success = useCallback(() => {
        history.push("/home")
    }, [history])


    useEffect(()=>{
        let token = sessionStorage.getItem("access-token");
        let client = sessionStorage.getItem("client");
        let uid = sessionStorage.getItem("uid");

        if (token && client && uid) {
            on_login_success();
        }
    }, [on_login_success])

    
    const try_login = (event) => {
        event.preventDefault();
        set_loading(true);
        login(email, password)
        .then(result => {
            if (!result.success) {
                set_error(true);
            }
            else {
                set_error(false);
                on_login_success();
            }
            set_loading(false);
        })
        .catch(err => {
            if (err && !err.success) {
                set_error(true);
            }
            else {
                set_error(false);
                on_login_success();
            }
            set_loading(false);
        })
    }

    const update_field = (value, setter) => {
        set_error(false);
        setter(value);
    }

    return(
        <Fragment>
            <Helmet>
                <title>Empresas - Login</title>
            </Helmet>
            <main className="background">
                <h1 className="hidden">Empresas - Login</h1>
                <img alt="Ioasys" src="/images/logo-home.png"></img>

                <div className="row mw-300">
                    <h2>BEM-VINDO AO EMPRESAS</h2>
                </div>
                <div className="row mw-300">
                    <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                </div>

                <form className="login-form" onSubmit={try_login}>
                    <TextInput marginTop={20} error={error} value={email} onInput={(e) => update_field(e, set_email)} Icon={GoMail} placeholder="E-mail"></TextInput>
                    <TextInput marginTop={20} error={error} value={password} onInput={(e) => update_field(e, set_password)} Icon={AiOutlineUnlock} placeholder="Senha" type="password"></TextInput>
                    {error && (<div className="row mw-300 error-message">
                        Credenciais informadas são inválidas, tente novamente.
                    </div>)}
                    <Button disabled={error} type="submit" text="ENTRAR"></Button>
                </form>

                <Loading visible={loading}/>
            </main>
        </Fragment>
    );
}

export default Login;