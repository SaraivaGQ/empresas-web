const api = "https://empresas.ioasys.com.br/api/v1"

function _req(route, method = "GET", body = {}) {
    let token = sessionStorage.getItem("access-token") || ""
    let client = sessionStorage.getItem("client") || ""
    let uid = sessionStorage.getItem("uid") || ""

    return new Promise((resolve, reject) => {
        let params = {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "access-token": token,
                "client": client,
                "uid": uid
            },
        }
        if (method !== "GET") {
            params.body = JSON.stringify(body)
        }

        fetch(api+route, params)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
}


function login(email, password) {
    return new Promise((resolve, reject) => {
        _req("/users/auth/sign_in", "POST", {email, password})
        .then(response => {
            if (response.status === 200) {
                sessionStorage.setItem("access-token", response.headers.get("access-token"));
                sessionStorage.setItem("client", response.headers.get("client"));
                sessionStorage.setItem("uid", response.headers.get("uid"));
            }
            response.json()
            .then(json=>{
                resolve(json)
            })
        })
        .catch(err => reject(err))
    })
}

function enterprises(filter = null) {
    return new Promise((resolve, reject) => {
        let url = "/enterprises"
        if (filter !== null) {
            url += "?name="+filter
        }
        _req(url, "GET")
        .then(response => {
            if (response.status === 401) {
                logoff();
            }

            response.json()
            .then(json => {
                resolve(json);
            })
        })
        .catch(err => {
            reject(err);
        })
    })
}


function get_enterprise(id) {
    return new Promise((resolve, reject) => {
        let url = "/enterprises/"+id
        _req(url, "GET")
        .then(response => {
            if (response.status === 401) {
                logoff();
            }

            response.json()
            .then(json => {
                resolve(json);
            })
        })
        .catch(err => {
            reject()
        })
    })
}

function is_logged() {
    let token = sessionStorage.getItem("access-token");
    let client = sessionStorage.getItem("client");
    let uid = sessionStorage.getItem("uid");
    return token && client && uid;
}   

function logoff() {
    sessionStorage.removeItem("access-token");
    sessionStorage.removeItem("client");
    sessionStorage.removeItem("uid");
    window.location.href = "/";
}

export {login, enterprises, get_enterprise, is_logged, logoff}