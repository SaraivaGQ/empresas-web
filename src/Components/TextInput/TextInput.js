import React, { useState } from 'react';
import "./TextInput.css"
import {AiFillEye, AiFillEyeInvisible} from 'react-icons/ai'

function TextInput({Icon, value, onInput, placeholder, error, type, theme, RightIcon, onRightIconClick, marginTop}) {

  const [visible, set_visibility] = useState(false);


  return(
      <div style={{marginTop:marginTop||0}} className={"input-area " + (error ? "error " : "") + (theme === 'white' ? "white" : "")}>
          {Icon && (
            <Icon className="icon"/>
          )}

          <input style={error &&  type === "password" ? {width: "calc(100% - 83px)"} : {}} value={value || ""} onInput={onInput ? (e) => onInput(e.target.value) : () => {}} type={visible ? "text" : type || "text"} placeholder={placeholder || ""}></input>

          {type === "password" && (
            visible ? <AiFillEye onClick={() => set_visibility(!visible)} className="eye"/> : <AiFillEyeInvisible onClick={() => set_visibility(!visible)} className="eye"/>
          )}

          {RightIcon && (
            <RightIcon className="icon clickable" onClick={() => onRightIconClick()}/>
          )}

          {error && (
            <div className={"alert-icon " + (error ? "error" : "")}>!</div>
          )}
      </div>
  );
}

export default TextInput;