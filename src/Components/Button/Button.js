import React from 'react';
import "./Button.css"

function Button({text, onClick, type, disabled}) {
  return <button disabled={disabled} type={type || "submit"} className="button-component" onClick={onClick ? onClick : () => {}}>{text}</button>;
}

export default Button;