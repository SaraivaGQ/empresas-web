import React from 'react';
import "./Loading.css";
import {AiOutlineLoading3Quarters} from 'react-icons/ai'

function Loading({visible}) {
  return(
      <>
        {visible && (
            <div className="loading-area">
                <AiOutlineLoading3Quarters className="icon"/>
            </div>
        )}
      </>
  );
}

export default Loading;