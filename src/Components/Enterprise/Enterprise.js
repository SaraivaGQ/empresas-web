import React from 'react';
import "./Enterprise.css";

import {Link} from 'react-router-dom';

function Enterprise({id, name, country, typeName, img, className, noLink}) {
  return(
    <div  className={"enterprise-card " + className}>
      {!noLink && (
        <Link to={id ? "/enterprise/"+id : null} className={"enterprise-link"}></Link>
      )}
      {img && (
        <img alt={name || "" + id || "" } src={img}></img>
      )}
      <div className="content">
          <h2>{name}</h2>
          <h3>{typeName}</h3>
          <p>{country}</p>
      </div>
      
    </div>
  );
}

export default Enterprise;