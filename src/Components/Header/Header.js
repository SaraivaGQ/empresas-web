import React, { useState } from 'react';
import "./Header.css";

import TextInput from '../TextInput/TextInput';

import {AiOutlineSearch, AiOutlineClose, AiOutlineArrowLeft} from 'react-icons/ai';
import {enterprises} from '../../services/api';
import {Link} from 'react-router-dom';

function Header({onSearch, isDetails, title}) {

  const [searching, set_searching] = useState(false);
  const [searchValue, set_search_value] = useState("");
  
  const search = (filter) => {
    set_search_value(filter);
    enterprises(filter)
    .then(result => {
      onSearch(result.enterprises);
    })
    .catch(err => {
      console.error(err);
    })
  }

  return(
      <header>
        {isDetails ?
        <>
        <Link to="/home" className="back-btn"><AiOutlineArrowLeft /></Link>
        <h1 className="header-title">{title}</h1>
        </>
        :
        <>
          <img height="48px" width="193px" alt="ioasys" src="/images/logo-nav@3x.png" className={" " + (searching ? "header-out" : "")}></img>
          {searching && (
            <div className="row search-box">
              <TextInput value={searchValue} marginTop={0} theme="white" onInput={search} Icon={AiOutlineSearch} placeholder="Pesquisar" RightIcon={AiOutlineClose} onRightIconClick={() => set_searching(false)}></TextInput>
            </div>
          )}
          <AiOutlineSearch  onClick={() => set_searching(!searching)} className={"search-btn clickable " + (searching ? "header-out" : "")}/>
        </>
        }
        
      </header>
  );
}

export default Header;